 var gsteganographyBundle = Components.classes["@mozilla.org/intl/stringbundle;1"].getService(Components.interfaces.nsIStringBundleService);
 var mystrings = gsteganographyBundle.createBundle("chrome://simplesteganography/locale/translations.properties");

 simpleSteganography = function () {
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);

	//var debugHint = '';
	var lastIndex = 0;
	var keyIndex = 0;
	var colorIndex = 0;
	var countBitsToProcess = 0;
	var key = '';
	var compressionMarker = String.fromCharCode(2);
	
	// action lists for asynchronous LSB
	var embedActions;
	var extractActions;

	// for Thunderbird
	var attachmentName;
	
	return {		
		init : function () {
			//
		},
		
		getKey : function () {
			key = prefManager.getCharPref("extensions.simplesteganography.key");
			enterPassphrase = prefManager.getBoolPref("extensions.simplesteganography.enter-passphrase");
			
			if(enterPassphrase || (key.length == 0))
			{
				key = content.prompt(mystrings.GetStringFromName("pleaseEnterYourPassphrase"), key);
			}
		},
		
		embedSelected : function (contextNode) {
			keyIndex = 0;
			this.getKey();
			var imageNodes = this.getSelectedImages(contextNode);
		
			if(imageNodes.length > 0)
			{
				// current selection contains an image
				var _this = this;
				var img = new Image();
				img.onload = function()
				{
					img.onload = null;					
					_this.embedIntoImage(imageNodes, img);
				}
				img.src = imageNodes[0].src;
			}
			else
			{
				// no image in current selection, extract plain text
				this.embedIntoText();
			}
		},

		// for Thunderbird: attach image file with stego message
		embedAttachImage : function() {
			keyIndex = 0;
			this.getKey();

			var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
			fp.init(window, getComposeBundle().getString("chooseFileToAttach"), nsIFilePicker.modeOpen);
			
			fp.appendFilters(nsIFilePicker.filterAll);
			
			if (fp.show() == nsIFilePicker.returnOK)
			{
				var _this = this;
				var img = new Image();
				img.onload = function()
				{
					img.onload = null;					
					_this.embedIntoImage(null, img);
				}
				attachmentName = fp.fileURL.path.split( '/' );
				attachmentName = attachmentName[attachmentName.length-1].split('.')[0]+'.png';
				img.src = fp.fileURL.spec;
			}
		},

		getImageCapacity : function(img) {
			// each LSB stores one bit
			var imageCapacity = (img.width * img.height) - 1;
			// we need 8 LSBs for one character
			imageCapacity = Math.floor(imageCapacity / 8);
			// 4 bytes are reserved for the message length
			imageCapacity = imageCapacity - 4;
			return(imageCapacity);
		},
		
		// Firefox uses [imageNodes],
		// Thunderbird uses [selectedImage]
		embedIntoImage : function (imageNodes, selectedImage) {
			colorIndex = 0;
			lastIndex = 0;
			countBitsProcessed = 0;
			var imageTextCapacity = 0;

			if(imageNodes == null) {
				imageNode = null;
			}else{
				imageNode = imageNodes[0];
			}

			imageTextCapacity = this.getImageCapacity(selectedImage);
			
			// enter message in text dialog
			var retVals = { textMessage: null, passphrase: null }; // passphrase reserved for future use
			var inputWin = window.openDialog("chrome://simplesteganography/content/messageDialog.xul", "", 
					  "chrome, dialog, modal", 
					  'get', imageTextCapacity, retVals).focus();
			var message = retVals.textMessage;
			
			if(message.length > imageTextCapacity) {
				message = message.substr(0, imageTextCapacity);
				alert('WARNING: The message is cropped to '+imageTextCapacity+' characters.');
			}
			
			// compress message to save pixels and/or make dictionary attacks more complicated
			var messageComp = compressionMarker + LZString.compressToBase64(message);
			if(messageComp.length < imageTextCapacity) {
				//For very short texts, the compressed string can be longer then the uncompressed one.
				message = messageComp;
			}

			var messageLength = message.length;

			var canvas = content.document.createElement('canvas');
			canvas.width = selectedImage.width;
			canvas.height = selectedImage.height;
			
			var context2d = canvas.getContext('2d');
			context2d.drawImage(selectedImage, 0, 0);
			//var imageData = context2d.getImageData(0, 0, 1, 1);
			
			// show waiting animation
			if(imageNode) {
				imageNode.src = 'chrome://simplesteganography/locale/pleaseWait_embed.gif';			
				content.document.body.style.cursor = 'progress';
			}
		
			embedActions = new Array();
			countBitsToProcess = (message.length + 4) * 8;
			
			// locate pixels to hide message size
			
			var countRemainingBits = imageTextCapacity * 8;
			var storeInEmbedActionIndex = 0;
			
			for(var charIndex = 0; charIndex < 4; charIndex++)
			{
				currentCharCode = this.getByte(messageLength, charIndex);
				
				embedActions[storeInEmbedActionIndex] = {
					pixelIndex : new Array(),
					clr : new Array(),
					value : currentCharCode
				}
				
				for(var bitIndex = 0; bitIndex < 8; bitIndex++)
				{
					this.prepareSetBitInNextPixel(countRemainingBits, bitIndex, storeInEmbedActionIndex, canvas.width, canvas.height);
					countRemainingBits--;
				}
				
				storeInEmbedActionIndex++;
			}
			
			// locate pixels to hide message content
			
			var i = 0;
			var currentBit = false;
			var countRemainingBits = message.length * 8;
			
			for(var charIndex = 0; charIndex < message.length; charIndex++)
			{	
				currentCharCode = message.charCodeAt(charIndex);
				
				embedActions[storeInEmbedActionIndex] = {
					pixelIndex : new Array(),
					clr : new Array(),
					value : currentCharCode
				}
				
				for(var bitIndex = 0; bitIndex < 8; bitIndex++)
				{
					this.prepareSetBitInNextPixel(countRemainingBits, bitIndex, storeInEmbedActionIndex, canvas.width, canvas.height);
					countRemainingBits--;
				}
				
				storeInEmbedActionIndex++;
			}
			
			// start embedding sequence
			var imageData = context2d.getImageData(0, 0, canvas.width, canvas.height);
			var _this = this;
			content.setTimeout(function(){ _this.asyncEmbedByteInPixels(0, imageNode, canvas, context2d, imageData); }, 100);
		},
		
		getSmallAlphabetCharCode : function(value, position) {
			var result = value.charCodeAt(position);
			
		        if (result > 96 && result < 123){
		            result = result - 97; // letters
		        } else if (result > 47 && result < 58){
		            result = result - 22; // numbers
		        } else if (result == 32){
		            result = 36; // space
		        }
			
			return(result);
		},
		
		getSmallAlphabetCharFromCode : function(value) {
			result = '';

			if (value < 26){
				result = String.fromCharCode(value + 97); // letters
			} else if ((value > 25)&&(value < 36)){
				result = String.fromCharCode(value + 22); // numbers
			} else if (value == 36){
				result = ' '; // space
			}
			
			return(result);
		},
		
		bigFactorial : function(value) {
			result = new bigInt(value);
			
			for(n = value-1; n > 0; n--) {
				result = result.times(n);
			}
			
			return(result);
		},
		
		getBitCount : function(value) {
			var bitIndex = value.toString().length;
			var bigTwo = new bigInt(2);
			var test = bigTwo.pow(bitIndex);
			
			while(value.greater(test)) {
				bitIndex++;
				test = bigTwo.pow(bitIndex);
			}
			
			return(bitIndex+1);
		},
		
		compareByKey : function(a, b) {
			if(a == b) {
				return(0);
			}
			if(a.length == 0) {
				return -1;
			}
			if(b.length == 0) {
				return 1;
			}
			
			var maxIndexA = a.length-1;
			var maxIndexB = b.length-1;
			
			var index = 0;
			while((index <= maxIndexA) && (index < maxIndexB) && (a.charAt(index) == b.charAt(index))) {
				index++;
			}
			
			if(index > maxIndexA) {
				return(-1)
			}
			if (index > maxIndexB) {
				return(1);
			}
			
			var indexA = key.indexOf(a.charAt(index));
			var indexB = key.indexOf(b.charAt(index));
			
			if((indexA < 0) && (indexB < 0)) {
				// both characters are missing in the passphrase -> sort alphabetically
				if(a < b) {
					return(-1);
				} else {
					return(1);
				}
			}
			if (indexA < 0) {
				return(1);
			}
			if (indexB < 0) {
				return(-1);
			}
			
			return(indexA - indexB);
		},
		
		textToCarrierList : function(text) {
			var list = text.split('\n');
			
			// remove empty items
			var n = 0;
			while(n < list.length) {
				list[n] = list[n].trim();
				if(list[n].length == 0) {
					list.splice(n, 1);
				} else {
					n++
				}
			}
			
			return(list);
		},

		embedIntoText : function() {
			keyIndex = 0;
			this.getKey();

			var carrierText;
			
			if(document.commandDispatcher.focusedWindow.getSelection) {
				carrierText = document.commandDispatcher.focusedWindow.getSelection().toString();
			} else {
				carrierText = content.window.getSelection().toString();
			}

			var capacitySorting = this.getTextCapacitySorting(carrierText);
			var capacitySpaces = this.getTextCapacitySpaces(carrierText);
			
			// enter message in text dialog
			var retVals = { textMessage: null, embeddingMethod: null, passphrase: null }; // passphrase reserved for future use
			var inputWin = window.openDialog("chrome://simplesteganography/content/promptMessageDialog.xul", "", 
					  "chrome, dialog, modal", 
					  'get', capacitySpaces, capacitySorting, retVals).focus();

			var text = retVals.textMessage;
			var result;

			if(retVals.embeddingMethod == 'sort'){
				result = this.embedIntoTextBySorting(carrierText, text);
			}else if(retVals.embeddingMethod == 'spaces'){
				result = this.embedIntoTextBySpaces(carrierText, text);
			}else{
				text1 = text.substr(0, capacitySorting);
				text2 = text.substr(capacitySorting, text.length-capacitySorting); 
				result = this.embedIntoTextBySorting(carrierText, text1);
				result = this.embedIntoTextBySpaces(result, text2);
			}

		  	// display result
			window.openDialog("chrome://simplesteganography/content/displayTextDialog.xul", "", 
					  "chrome, dialog, modal, resizable=yes", 
					  result).focus();
		},

		getTextCapacitySorting : function(carrierText) {
			var sortedItems = this.textToCarrierList(carrierText);
			
			// use alphabet with up to 64 characters (a-z 0-9 [space])
			var bitsPerChar = 6; 
			var charsInAlphabet = bigInt(37);
			
			var maxMessageBitCount = this.bigFactorial(sortedItems.length);
			var maxMessageBitCount = this.getBitCount(maxMessageBitCount);
			
			return Math.floor(maxMessageBitCount / bitsPerChar) - 1;
		},

		getTextCapacitySpaces : function(carrierText) {
			var bitsPerLine = 4;
			var carrierLines = carrierText.split('\n');
			return Math.floor(carrierLines.length * bitsPerLine / 8);
		},

		embedIntoTextBySpaces : function(carrierText, text) {
			var carrierLines = carrierText.split('\n');
			var lineIndex = 0;

			//var capacity = this.getTextCapacitySpaces(carrierLines);
			//var text = content.prompt('Please enter up to '+capacity+' characters.');

			for(n=0; ((n<text.length)&&(lineIndex<carrierLines.length)); n++) {
				charCode = text.charCodeAt(n);
				charCode = (charCode ^ this.getKeyValue()) + 1; // charCode may not be 0

				var value = this.getLowByte(charCode);
				var linePostfix = this.valueToSpaces(value);
				carrierLines[lineIndex] = carrierLines[lineIndex].trim() + linePostfix;
				lineIndex++;

				value = this.getHighByte(charCode);
				linePostfix = this.valueToSpaces(value);
				carrierLines[lineIndex] = carrierLines[lineIndex].trim() + linePostfix;
				lineIndex++;
			}

			carrierText = carrierLines.join('\n');

			return carrierText;
		},

		embedIntoTextBySorting : function(carrierText, text) {
			// sort original text alphabetically
			var sortedItems = this.textToCarrierList(carrierText);
			sortedItems.sort(this.compareByKey);
			
			// use alphabet with up to 64 characters (a-z 0-9 [space])
			var charsInAlphabet = bigInt(37);
						
			//var capacity = getTextCapacitySorting(carrierText);
			//var text = content.prompt('Please enter up to '+capacity+' characters, only a-z 0-9 [space].');
			var message = bigInt(0);
			
			// convert message string to big integer, each letter as a digit
			
			for(n=0; n<text.length; n++) {
				charCode = this.getSmallAlphabetCharCode(text, n);
				var charValue = charsInAlphabet.pow(text.length - 1 - n);
                            	charValue = charValue.times(charCode);
                            	message = message.add(charValue);			
			}
			
			// initialize carrier
			var freeIndexes = new Array();
			var result = new Array();
			var skip = 0;
			var resultIndex = 0;
			
			for(n=0; n<sortedItems.length; n++) {
				freeIndexes[n] = n;
				result[n] = ' ';
			}
			
			for (indexSource = 0; indexSource < sortedItems.length; indexSource++)
			{
				divResult = message.divmod(freeIndexes.length);
				skip = divResult.remainder.valueOf();
				message = divResult.quotient;
				resultIndex = freeIndexes[skip];
				result[resultIndex] = sortedItems[indexSource];
				freeIndexes.splice(skip, 1);
		  	}
		  	
			return result.join('\n');
		},
		
		extractSelected : function (contextNode) {
			this.getKey();
			var imageNodes = this.getSelectedImages(contextNode);
			
			if(imageNodes.length > 0)
			{
				var _this = this;
				var img = new Image();
				img.onload = function()
				{
					img.onload = null;					
					_this.extractFromImage(imageNodes, img);
				}
				img.src = imageNodes[0].src;

			} else {
				this.extractFromText();
			}
		},
		
		extractFromImage : function(imageNodes, selectedImage) {
			//debugHint = '';
			content.document.body.style.cursor = 'progress';
			
			lastIndex = 0;
			keyIndex = 0;
			colorIndex = 0;
			var message = '';
			
			var canvas = content.document.createElement('canvas');
			canvas.width = selectedImage.width;
			canvas.height = selectedImage.height;
			
			var context2d = canvas.getContext('2d');
			context2d.drawImage(selectedImage, 0, 0);
			
			// show waiting animation
			imageNodes[0].src = 'chrome://simplesteganography/locale/pleaseWait_extract.gif';
				
			var imageTextCapacity = (selectedImage.width * selectedImage.height) - 1;
			imageTextCapacity = Math.floor(imageTextCapacity / 8) - 4;
		
			extractActions = new Array();
			var countRemainingBits = imageTextCapacity * 8;
			var messageLength = 0;
			var currentCharCode = 0;
			
			for(index=0; index<4; index++) {
				this.prepareGetByteFromNextPixels(countRemainingBits, selectedImage.width, selectedImage.height, index);
				countRemainingBits -= 8;
			}

			// start extract sequence
			var imageData = context2d.getImageData(0, 0, canvas.width, canvas.height);			
			//var imageData = context2d.getImageData(0, 0, selectedImage.width, selectedImage.height);
			var _this = this;
			content.setTimeout(function(){ _this.asyncExtractByteFromPixels(0, imageNodes[0], context2d, imageData, false); }, 100);
		},
		
		extractFromText: function() {
			keyIndex = 0;
			this.getKey();

			if(document.commandDispatcher.focusedWindow.getSelection) {
				carrierText = document.commandDispatcher.focusedWindow.getSelection().getRangeAt(0).toString();
			} else {
				carrierText = content.window.getSelection().toString();
			}
			
			var text1 = this.extractFromTextBySpaces(carrierText);
			var text2 = this.extractFromTextBySorting(carrierText);
			var text = text2 + text1;

		  	// display result
			var retVals = { textMessage: text, passphrase: null };
			var inputWin = window.openDialog("chrome://simplesteganography/content/messageDialog.xul", "", 
							 "chrome, dialog, modal, resizable=no", 
							 'show', 0, retVals).focus();
		},

		extractFromTextBySpaces: function(carrierText) {
			var carrierList = carrierText.split('\n');
			var text = '';
			
			for(var n=0; n<carrierList.length-1; n+=2) {
				var lineValue = this.valueFromSpaces(carrierList[n]);
				var charCode = lineValue;

				lineValue = this.valueFromSpaces(carrierList[n+1]);
				charCode += (lineValue * 16);

				if(charCode > 0) {
					charCode = (charCode - 1) ^ this.getKeyValue();
					text += String.fromCharCode(charCode);
				}
			}

		  	return text;
		},

		extractFromTextBySorting: function(carrierText) {
			var carrier = this.textToCarrierList(carrierText);
			var sortedItems = carrier.slice(0);
			sortedItems.sort(this.compareByKey);
			
			// use alphabet with 37 characters (a-z 0-9 [space])
			var charsInAlphabet = 37;
			message = new bigInt(0);

			for (carrierIndex = 0; carrierIndex < sortedItems.length; carrierIndex++)
			{
				var itemOrdinal = carrier.indexOf(sortedItems[carrierIndex]);
				var skip = 0;
				
				for (var countIndex = 0; countIndex < carrierIndex; countIndex++)
				{
				    if (this.compareByKey(carrier[countIndex], carrier[carrierIndex]) > 0)
				    {   // There is a bigger item to the left. It's place
				        // must have been skipped by the current item.
				        skip++;
				    }
				}

				// Revert the division that resulted in this skip value
				var itemOrdinal = sortedItems.indexOf(carrier[carrierIndex])+1;
				var value = new bigInt(skip);
				for (var countIndex = 1; countIndex < itemOrdinal; countIndex++)
				{
				    value = value.times(sortedItems.length - countIndex + 1);
				}
				
				message = message.add(value);
			}
			
			// convert integer to text
			var text = '';
			var divResult;
			
			var temp = new bigInt(message);
			var maxExp = -1;
			while(temp.greater(0)){
				maxExp++;
            			temp = temp.divmod(charsInAlphabet).quotient;
			}
			
			for(var n=maxExp; n>-1; n--){
                    		positionValue = new bigInt(charsInAlphabet).pow(n);	
				divResult = message.divmod(positionValue);
				charValue = divResult.quotient;
                		message = divResult.remainder;
		                text += this.getSmallAlphabetCharFromCode(charValue.valueOf());
			}
			
			return text;
		},
		
		getHighByte : function(value) {
			var mask = 240;
			return ((value & mask) >> 4);
		},
		
		clearLowByte : function(value) {
			var mask = 240;
			return (value & mask);
		},
		
		getLowByte : function(value) {
			var mask = 15;
			return (value & mask);
		},
		
		getByte : function(intValue, byteIndex) {
			var mask = 255 << (byteIndex*8);
			result = (intValue & mask);
			result = result >> (byteIndex*8);
			return result;
		},		
		
		setByte : function(intValue, newByte, byteIndex) {
			var part = newByte << (byteIndex*8);
			return (intValue + part);
		},

		valueToSpaces : function(value) {
			var tabs = Math.floor(value / 4);
			var spaces = value - (tabs * 4)
			var linePostfix = ' '.repeat(spaces) + '\t'.repeat(tabs);
			return linePostfix;
		},

		valueFromSpaces : function(line) {
			var charIndex = line.length-1;
			var lineValue = 0;
			
			while(charIndex > -1) {
				if(line[charIndex] == ' '){
					lineValue += 1;
				}
				else if(line[charIndex] == '\t'){
					lineValue += 4;
				} else {
					break;
				}
				charIndex--;
			}

			return lineValue;
		},
		
		findImageNodes : function (parentNode, outResultNodes) {
			for(var i=0; i<parentNode.childNodes.length; i++)
			{
				if(parentNode.childNodes[i].nodeName == 'img')
				{
					outResultNodes[outResultNodes.length] = parentNode.childNodes[i];	
				} 
				else if(parentNode.childNodes[i].nodeType == 1) 
				{
					this.findImageNodes(parentNode.childNodes[i], outResultNodes);
				}
			}
		},

		getSelectedImages : function (contextNode)
		{
			var imageNodes = new Array();
			
			if(contextNode.nodeName.toLowerCase() == 'img')
			{
				imageNodes[0] = contextNode;
			}
			else
			{
				var selection = content.getSelection();

				if(selection.rangeCount > 0)
				{
					selectedContents = selection.getRangeAt(0).cloneContents();
					this.findImageNodes(selectedContents, imageNodes);
				}
			}
			return imageNodes;
		},

		getKeyValue : function () {
			keyIndex++;
			if(keyIndex >= key.length)
			{
				keyIndex = 0;
			}
			return key.charCodeAt(keyIndex);
		},

		getNextColor : function (imageData) {
			colorIndex++;
			if(colorIndex >= 3) 
			{
				colorIndex = 0;
			}
	
			return imageData.data[colorIndex];
		},

		getCurrentColor : function (imageData) {
			return imageData.data[colorIndex];
		},
		
		getBitFromColor : function (colorValue) {
			var isEven = ((colorValue/2) == Math.round(colorValue/2));
			return !isEven;
		},

		prepareGetBitFromNextPixel : function (messageLength, currentBitIndex, imgWidth, imgHeight, storeInExtractActionIndex) {
			var maxIndex = (imgWidth * imgHeight) - 1;
			var nextIndex;
			var countRemainingPixels = maxIndex - lastIndex;
			var keyValue = this.getKeyValue();
	
			var blockSize = Math.floor(countRemainingPixels / messageLength);
			if(blockSize < 2)
			{
				nextIndex = lastIndex + 1;
			} else {
				nextIndex = lastIndex + ((keyValue % (blockSize-1)) + 1);
			}
				
			extractActions[storeInExtractActionIndex].pixelIndex[currentBitIndex] = nextIndex;
			extractActions[storeInExtractActionIndex].clr[currentBitIndex] = colorIndex;
					
			colorIndex++;
			if(colorIndex >= 3) 
			{
				colorIndex = 0;
			}
			
			lastIndex = nextIndex;
		},
		
		asyncExtractByteFromPixels : function(extractActionIndex, img, context2d, imageData, extractMessage) {
			
			extractActions[extractActionIndex].result = 0;
						
			for(var bitIndex=0; bitIndex<8; bitIndex++) 
			{			
				var pixelIndex = extractActions[extractActionIndex].pixelIndex[bitIndex];
				var clr = extractActions[extractActionIndex].clr[bitIndex];
						
				var colorValue = imageData.data[(pixelIndex * 4) + clr];
				var messageBit = !((colorValue/2) == Math.round(colorValue/2));
				//debugHint += '('+pixelIndex+'/'+clr+'/'+colorValue+')'+' ';
				
				if(messageBit)
				{
					extractActions[extractActionIndex].result += Math.pow(2, bitIndex);
				}
			}
			
			extractActionIndex++;
			
			if(extractActionIndex < extractActions.length) 
			{
				var _this = this;
				content.setTimeout(function(){ _this.asyncExtractByteFromPixels(extractActionIndex, img, context2d, imageData, extractMessage); }, 100);
			} else {
				if(extractMessage) {
					var message = '';
					
					//content.prompt('', debugHint);
					
					for(var index=0; index<extractActions.length; index++) {
						message += String.fromCharCode(extractActions[index].result);
					}
					
					// reset image
					img.src = context2d.canvas.toDataURL("image/png");
					
					// decompress message
					if(message.charAt(0) == compressionMarker) {
						message = message.slice(1, message.length);
						message = LZString.decompressFromBase64(message);
					}
					
					// display message in text dialog
					content.document.body.style.cursor = 'default';
					var retVals = { textMessage: message, passphrase: null };
					var inputWin = window.openDialog("chrome://simplesteganography/content/messageDialog.xul", "", 
							  "chrome, dialog, modal, resizable=no", 
							  'show', 0, retVals).focus();
			
				} else {
					//content.prompt('', debugHint);
					
					// reconstruct length of message from first 4 bytes
					messageLength = 0;
					for(var index=0; index<extractActions.length; index++) {
						messageLength = this.setByte(messageLength, extractActions[index].result, index);
					}
					
					var imageTextCapacity = this.getImageCapacity(img);
					
					// locate pixels with message-bits
					if((messageLength > 0)||(messageLength > imageTextCapacity))
					{
						extractActions = new Array();
						var countRemainingBits = messageLength * 8;
					
						for(var index=0; index<messageLength; index++) {
							this.prepareGetCharFromNextPixels(countRemainingBits, context2d.canvas.width, context2d.canvas.height, index);
							countRemainingBits -= 8;
						}
			
						// start extract sequence
						var _this = this;
						content.setTimeout(function(){ _this.asyncExtractByteFromPixels(0, img, context2d, imageData, true); }, 100);
					} else {
						alert(mystrings.GetStringFromName("thereIsNoMessageFound"));
						// reset image
						img.src = context2d.canvas.toDataURL("image/png");
					}
				}
			}
		},

		attachImage : function(imageDataUrl) {
			Components.utils.import("resource://gre/modules/FileUtils.jsm");
			Components.utils.import("resource://gre/modules/NetUtil.jsm");

			// get image as stream
			var nsIoService = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService);
			var channel = nsIoService.newChannelFromURI(nsIoService.newURI(imageDataUrl, null, null));
			var binaryInputStream = Components.classes["@mozilla.org/binaryinputstream;1"].createInstance(Components.interfaces.nsIBinaryInputStream);
			binaryInputStream.setInputStream(channel.open());

			// get temporary file
			var file = FileUtils.getFile("TmpD", "temp.png");
			file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, FileUtils.PERMS_FILE);
			var fileOutputStream = Components.classes["@mozilla.org/network/file-output-stream;1"].createInstance(Components.interfaces.nsIFileOutputStream);
			fileOutputStream.init(file, 0x02 | 0x08 | 0x20, parseInt("0600", 8), null);

			// write temporary file
			var numBytes = binaryInputStream.available();
			var bytes = binaryInputStream.readBytes(numBytes);
			fileOutputStream.write(bytes, numBytes);
			fileOutputStream.close();

			// prepare attachment
			var attachment = FileToAttachment(file);
			attachment.contentType = "image/png";
			attachment.name = attachmentName;

			// attach temporary file
			var attachments = [];
			attachments.push(attachment);
			AddAttachments(attachments);
		},
		
		// only Firefox uses [imgNode]
		asyncEmbedByteInPixels : function(embedActionIndex, imgNode, canvas, context2d, imageData) {
			for(var bitIndex=0; bitIndex<8; bitIndex++) 
			{			
				var pixelIndex = embedActions[embedActionIndex].pixelIndex[bitIndex]; 
				var clr = embedActions[embedActionIndex].clr[bitIndex];
				
				var colorValue = imageData.data[(pixelIndex*4) + clr];
				var originalBit = this.getBitFromColor(colorValue);
				var bit = ((embedActions[embedActionIndex].value & Math.pow(2, bitIndex)) > 0);

				if(originalBit != bit) {
					if(bit) {
						colorValue += 1;
					} else {
						colorValue -= 1;
					}
				}
	
				// change LSB
				imageData.data[(pixelIndex*4) + clr] = colorValue;
				// remove transparency
				imageData.data[(pixelIndex*4) + 3] = 255;
			}
			
			embedActionIndex++;
			
			if(embedActionIndex < embedActions.length) 
			{
				var _this = this;
				content.setTimeout(function(){ _this.asyncEmbedByteInPixels(embedActionIndex, imgNode, canvas, context2d, imageData); }, 100);
			} else {
				context2d.putImageData(imageData, 0, 0);
				var imageDataUrl = canvas.toDataURL("image/png");
				
				if(imgNode)
				{
					content.document.body.style.cursor = 'default';
					this.saveAsPNG(canvas);
					imgNode.src = imageDataUrl;
				} else {
					this.attachImage(imageDataUrl);
				}
			}
		},
		
		prepareSetBitInNextPixel : function (messageLength, currentBitIndex, storeInEmbedActionIndex, imgWidth, imgHeight) {
			var maxIndex = (imgWidth * imgHeight) - 1;
			var countRemainingPixels = maxIndex - lastIndex;
			var keyValue = this.getKeyValue();
			var blockSize = Math.floor(countRemainingPixels / messageLength);
			
			var lastIndexInBlock = lastIndex + blockSize;
			var nextIndex;
			
			if(blockSize < 2)
			{
				nextIndex = lastIndex + 1;
			} else {
				nextIndex = lastIndex + ((keyValue % (blockSize-1)) + 1);
			}

			embedActions[storeInEmbedActionIndex].pixelIndex[currentBitIndex] = nextIndex;
			embedActions[storeInEmbedActionIndex].clr[currentBitIndex] = colorIndex;
			
			colorIndex++;
			if(colorIndex >= 3) 
			{
				colorIndex = 0;
			}
			
			//debugHint += ' ('+nextX+','+nextY+')='+colorValue+'/'+colorIndex+' ';	
			
			lastIndex = nextIndex;
		},

		prepareGetByteFromNextPixels : function (messageLength, imgWidth, imgHeight, storeInExtractActionIndex) {
			extractActions[storeInExtractActionIndex] = {
				pixelIndex : new Array(),
				clr : new Array(),
				result : 0
			}
	
			for(var bitIndex = 0; bitIndex < 8; bitIndex++)
			{
				this.prepareGetBitFromNextPixel((messageLength-bitIndex), bitIndex, imgWidth, imgHeight, storeInExtractActionIndex);
			}
		},

		prepareGetCharFromNextPixels : function (messageLength, imgWidth, imgHeight, context2d) {
			var charCode = this.prepareGetByteFromNextPixels(messageLength, imgWidth, imgHeight, context2d);
			return String.fromCharCode(charCode);
		},
		
		saveAsPNG : function(canvas) {
			var dataUrl = canvas.toDataURL("image/png");
			document.location.href = dataUrl.replace('image/png', 'image/octet-stream');
			return true;
		}
	};	
}();

/* 
Free Big Integer Class by Peter Olson
Source: https://npmjs.org/package/big-integer 
*/
var bigInt = (function () {
    var base = 10000000, logBase = 7;
    var sign = {
        positive: false,
        negative: true
    };

    var normalize = function (first, second) {
        var a = first.value, b = second.value;
        var length = a.length > b.length ? a.length : b.length;
        for (var i = 0; i < length; i++) {
            a[i] = a[i] || 0;
            b[i] = b[i] || 0;
        }
        for (var i = length - 1; i >= 0; i--) {
            if (a[i] === 0 && b[i] === 0) {
                a.pop();
                b.pop();
            } else break;
        }
        if (!a.length) a = [0], b = [0];
        first.value = a;
        second.value = b;
    };

    var parse = function (text, first) {
        if (typeof text === "object") return text;
        text += "";
        var s = sign.positive, value = [];
        if (text[0] === "-") {
            s = sign.negative;
            text = text.slice(1);
        }
        var text = text.split("e");
        if (text.length > 2) throw new Error("Invalid integer");
        if (text[1]) {
            var exp = text[1];
            if (exp[0] === "+") exp = exp.slice(1);
            exp = parse(exp);
            if (exp.lesser(0)) throw new Error("Cannot include negative exponent part for integers");
            while (exp.notEquals(0)) {
                text[0] += "0";
                exp = exp.prev();
            }
        }
        text = text[0];
        if (text === "-0") text = "0";
        var isValid = /^([0-9][0-9]*)$/.test(text);
        if (!isValid) throw new Error("Invalid integer");
        while (text.length) {
            var divider = text.length > logBase ? text.length - logBase : 0;
            value.push(+text.slice(divider));
            text = text.slice(0, divider);
        }
        var val = bigInt(value, s);
        if (first) normalize(first, val);
        return val;
    };

    var goesInto = function (a, b) {
        var a = bigInt(a, sign.positive), b = bigInt(b, sign.positive);
        if (a.equals(0)) throw new Error("Cannot divide by 0");
        var n = 0;
        do {
            var inc = 1;
            var c = bigInt(a.value, sign.positive), t = c.times(10);
            while (t.lesser(b)) {
                c = t;
                inc *= 10;
                t = t.times(10);
            }
            while (c.lesserOrEquals(b)) {
                b = b.minus(c);
                n += inc;
            }
        } while (a.lesserOrEquals(b));

        return {
            remainder: b.value,
            result: n
        };
    };

    var bigInt = function (value, s) {
        var self = {
            value: value,
            sign: s
        };
        var o = {
            value: value,
            sign: s,
            negate: function (m) {
                var first = m || self;
                return bigInt(first.value, !first.sign);
            },
            abs: function (m) {
                var first = m || self;
                return bigInt(first.value, sign.positive);
            },
            add: function (n, m) {
                var s, first = self, second;
                if (m) (first = parse(n)) && (second = parse(m));
                else second = parse(n, first);
                s = first.sign;
                if (first.sign !== second.sign) {
                    first = bigInt(first.value, sign.positive);
                    second = bigInt(second.value, sign.positive);
                    return s === sign.positive ?
						o.subtract(first, second) :
						o.subtract(second, first);
                }
                normalize(first, second);
                var a = first.value, b = second.value;
                var result = [],
					carry = 0;
                for (var i = 0; i < a.length || carry > 0; i++) {
                    var sum = (a[i] || 0) + (b[i] || 0) + carry;
                    carry = sum >= base ? 1 : 0;
                    sum -= carry * base;
                    result.push(sum);
                }
                return bigInt(result, s);
            },
            plus: function (n, m) {
                return o.add(n, m);
            },
            subtract: function (n, m) {
                var first = self, second;
                if (m) (first = parse(n)) && (second = parse(m));
                else second = parse(n, first);
                if (first.sign !== second.sign) return o.add(first, o.negate(second));
                if (first.sign === sign.negative) return o.subtract(o.negate(second), o.negate(first));
                if (o.compare(first, second) === -1) return o.negate(o.subtract(second, first));
                var a = first.value, b = second.value;
                var result = [],
					borrow = 0;
                for (var i = 0; i < a.length; i++) {
                    a[i] -= borrow;
                    borrow = a[i] < b[i] ? 1 : 0;
                    var minuend = (borrow * base) + a[i] - b[i];
                    result.push(minuend);
                }
                return bigInt(result, sign.positive);
            },
            minus: function (n, m) {
                return o.subtract(n, m);
            },
            multiply: function (n, m) {
                var s, first = self, second;
                if (m) (first = parse(n)) && (second = parse(m));
                else second = parse(n, first);
                s = first.sign !== second.sign;
                var a = first.value, b = second.value;
                var resultSum = [];
                for (var i = 0; i < a.length; i++) {
                    resultSum[i] = [];
                    var j = i;
                    while (j--) {
                        resultSum[i].push(0);
                    }
                }
                var carry = 0;
                for (var i = 0; i < a.length; i++) {
                    var x = a[i];
                    for (var j = 0; j < b.length || carry > 0; j++) {
                        var y = b[j];
                        var product = y ? (x * y) + carry : carry;
                        carry = product > base ? Math.floor(product / base) : 0;
                        product -= carry * base;
                        resultSum[i].push(product);
                    }
                }
                var max = -1;
                for (var i = 0; i < resultSum.length; i++) {
                    var len = resultSum[i].length;
                    if (len > max) max = len;
                }
                var result = [], carry = 0;
                for (var i = 0; i < max || carry > 0; i++) {
                    var sum = carry;
                    for (var j = 0; j < resultSum.length; j++) {
                        sum += resultSum[j][i] || 0;
                    }
                    carry = sum > base ? Math.floor(sum / base) : 0;
                    sum -= carry * base;
                    result.push(sum);
                }
                return bigInt(result, s);
            },
            times: function (n, m) {
                return o.multiply(n, m);
            },
            divmod: function (n, m) {
                var s, first = self, second;
                if (m) (first = parse(n)) && (second = parse(m));
                else second = parse(n, first);
                s = first.sign !== second.sign;
                if (bigInt(first.value, first.sign).equals(0)) return {
                    quotient: bigInt([0], sign.positive),
                    remainder: bigInt([0], sign.positive)
                };
                if (second.equals(0)) throw new Error("Cannot divide by zero");
                var a = first.value, b = second.value;
                var result = [], remainder = [];
                for (var i = a.length - 1; i >= 0; i--) {
                    var n = [a[i]].concat(remainder);
                    var quotient = goesInto(b, n);
                    result.push(quotient.result);
                    remainder = quotient.remainder;
                }
                result.reverse();
                return {
                    quotient: bigInt(result, s),
                    remainder: bigInt(remainder, first.sign)
                };
            },
            divide: function (n, m) {
                return o.divmod(n, m).quotient;
            },
            over: function (n, m) {
                return o.divide(n, m);
            },
            mod: function (n, m) {
                return o.divmod(n, m).remainder;
            },
            pow: function (n, m) {
                var first = self, second;
                if (m) (first = parse(n)) && (second = parse(m));
                else second = parse(n, first);
                var a = first, b = second;
                if (b.lesser(0)) return ZERO;
                if (b.equals(0)) return ONE;
                var result = bigInt(a.value, a.sign);

                if (b.mod(2).equals(0)) {
                    var c = result.pow(b.over(2));
                    return c.times(c);
                } else {
                    return result.times(result.pow(b.minus(1)));
                }
            },
            next: function (m) {
                var first = m || self;
                return o.add(first, 1);
            },
            prev: function (m) {
                var first = m || self;
                return o.subtract(first, 1);
            },
            compare: function (n, m) {
                var first = self, second;
                if (m) (first = parse(n)) && (second = parse(m, first));
                else second = parse(n, first);
                normalize(first, second);
                if (first.value.length === 1 && second.value.length === 1 && first.value[0] === 0 && second.value[0] === 0) return 0;
                if (second.sign !== first.sign) return first.sign === sign.positive ? 1 : -1;
                var multiplier = first.sign === sign.positive ? 1 : -1;
                var a = first.value, b = second.value;
                for (var i = a.length - 1; i >= 0; i--) {
                    if (a[i] > b[i]) return 1 * multiplier;
                    if (b[i] > a[i]) return -1 * multiplier;
                }
                return 0;
            },
            compareAbs: function (n, m) {
                var first = self, second;
                if (m) (first = parse(n)) && (second = parse(m, first));
                else second = parse(n, first);
                first.sign = second.sign = sign.positive;
                return o.compare(first, second);
            },
            equals: function (n, m) {
                return o.compare(n, m) === 0;
            },
            notEquals: function (n, m) {
                return !o.equals(n, m);
            },
            lesser: function (n, m) {
                return o.compare(n, m) < 0;
            },
            greater: function (n, m) {
                return o.compare(n, m) > 0;
            },
            greaterOrEquals: function (n, m) {
                return o.compare(n, m) >= 0;
            },
            lesserOrEquals: function (n, m) {
                return o.compare(n, m) <= 0;
            },
            isPositive: function (m) {
                var first = m || self;
                return first.sign === sign.positive;
            },
            isNegative: function (m) {
                var first = m || self;
                return first.sign === sign.negative;
            },
            isEven: function (m) {
                var first = m || self;
                return first.value[0] % 2 === 0;
            },
            isOdd: function (m) {
                var first = m || self;
                return first.value[0] % 2 === 1;
            },
            toString: function (m) {
                var first = m || self;
                var str = "", len = first.value.length;
                while (len--) {
                    if (first.value[len].toString().length === 8) str += first.value[len];
                    else str += (base.toString() + first.value[len]).slice(-logBase);
                }
                while (str[0] === "0") {
                    str = str.slice(1);
                }
                if (!str.length) str = "0";
                var s = first.sign === sign.positive ? "" : "-";
                return s + str;
            },
            toJSNumber: function (m) {
                return +o.toString(m);
            },
            valueOf: function (m) {
                return o.toJSNumber(m);
            }
        };
        return o;
    };

    var ZERO = bigInt([0], sign.positive);
    var ONE = bigInt([1], sign.positive);
    var MINUS_ONE = bigInt([1], sign.negative);

    var fnReturn = function (a) {
        if (typeof a === "undefined") return ZERO;
        return parse(a);
    };
    fnReturn.zero = ZERO;
    fnReturn.one = ONE;
    fnReturn.minusOne = MINUS_ONE;
    return fnReturn;
})();

if (typeof module !== "undefined") {
    module.exports = bigInt;
}

// LZ-based compression algorithm, version 1.3.3
//
// Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
// This work is free. You can redistribute it and/or modify it
// under the terms of the WTFPL, Version 2
// For more information see LICENSE.txt or http://www.wtfpl.net/
//
// For more information, the home page:
// http://pieroxy.net/blog/pages/lz-string/testing.html

var LZString = {
  
  // private property
  _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
  _f : String.fromCharCode,
  
  compressToBase64 : function (input) {
    if (input == null) return "";
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;
    
    input = LZString.compress(input);
    
    while (i < input.length*2) {
      
      if (i%2==0) {
        chr1 = input.charCodeAt(i/2) >> 8;
        chr2 = input.charCodeAt(i/2) & 255;
        if (i/2+1 < input.length) 
          chr3 = input.charCodeAt(i/2+1) >> 8;
        else 
          chr3 = NaN;
      } else {
        chr1 = input.charCodeAt((i-1)/2) & 255;
        if ((i+1)/2 < input.length) {
          chr2 = input.charCodeAt((i+1)/2) >> 8;
          chr3 = input.charCodeAt((i+1)/2) & 255;
        } else 
          chr2=chr3=NaN;
      }
      i+=3;
      
      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
      
      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }
      
      output = output +
        LZString._keyStr.charAt(enc1) + LZString._keyStr.charAt(enc2) +
          LZString._keyStr.charAt(enc3) + LZString._keyStr.charAt(enc4);
      
    }
    
    return output;
  },
  
  decompressFromBase64 : function (input) {
    if (input == null) return "";
    var output = "",
        ol = 0, 
        output_,
        chr1, chr2, chr3,
        enc1, enc2, enc3, enc4,
        i = 0, f=LZString._f;
    
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    
    while (i < input.length) {
      
      enc1 = LZString._keyStr.indexOf(input.charAt(i++));
      enc2 = LZString._keyStr.indexOf(input.charAt(i++));
      enc3 = LZString._keyStr.indexOf(input.charAt(i++));
      enc4 = LZString._keyStr.indexOf(input.charAt(i++));
      
      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;
      
      if (ol%2==0) {
        output_ = chr1 << 8;
        
        if (enc3 != 64) {
          output += f(output_ | chr2);
        }
        if (enc4 != 64) {
          output_ = chr3 << 8;
        }
      } else {
        output = output + f(output_ | chr1);
        
        if (enc3 != 64) {
          output_ = chr2 << 8;
        }
        if (enc4 != 64) {
          output += f(output_ | chr3);
        }
      }
      ol+=3;
    }
    
    return LZString.decompress(output);
    
  },

  compressToUTF16 : function (input) {
    if (input == null) return "";
    var output = "",
        i,c,
        current,
        status = 0,
        f = LZString._f;
    
    input = LZString.compress(input);
    
    for (i=0 ; i<input.length ; i++) {
      c = input.charCodeAt(i);
      switch (status++) {
        case 0:
          output += f((c >> 1)+32);
          current = (c & 1) << 14;
          break;
        case 1:
          output += f((current + (c >> 2))+32);
          current = (c & 3) << 13;
          break;
        case 2:
          output += f((current + (c >> 3))+32);
          current = (c & 7) << 12;
          break;
        case 3:
          output += f((current + (c >> 4))+32);
          current = (c & 15) << 11;
          break;
        case 4:
          output += f((current + (c >> 5))+32);
          current = (c & 31) << 10;
          break;
        case 5:
          output += f((current + (c >> 6))+32);
          current = (c & 63) << 9;
          break;
        case 6:
          output += f((current + (c >> 7))+32);
          current = (c & 127) << 8;
          break;
        case 7:
          output += f((current + (c >> 8))+32);
          current = (c & 255) << 7;
          break;
        case 8:
          output += f((current + (c >> 9))+32);
          current = (c & 511) << 6;
          break;
        case 9:
          output += f((current + (c >> 10))+32);
          current = (c & 1023) << 5;
          break;
        case 10:
          output += f((current + (c >> 11))+32);
          current = (c & 2047) << 4;
          break;
        case 11:
          output += f((current + (c >> 12))+32);
          current = (c & 4095) << 3;
          break;
        case 12:
          output += f((current + (c >> 13))+32);
          current = (c & 8191) << 2;
          break;
        case 13:
          output += f((current + (c >> 14))+32);
          current = (c & 16383) << 1;
          break;
        case 14:
          output += f((current + (c >> 15))+32, (c & 32767)+32);
          status = 0;
          break;
      }
    }
    
    return output + f(current + 32);
  },
  

  decompressFromUTF16 : function (input) {
    if (input == null) return "";
    var output = "",
        current,c,
        status=0,
        i = 0,
        f = LZString._f;
    
    while (i < input.length) {
      c = input.charCodeAt(i) - 32;
      
      switch (status++) {
        case 0:
          current = c << 1;
          break;
        case 1:
          output += f(current | (c >> 14));
          current = (c&16383) << 2;
          break;
        case 2:
          output += f(current | (c >> 13));
          current = (c&8191) << 3;
          break;
        case 3:
          output += f(current | (c >> 12));
          current = (c&4095) << 4;
          break;
        case 4:
          output += f(current | (c >> 11));
          current = (c&2047) << 5;
          break;
        case 5:
          output += f(current | (c >> 10));
          current = (c&1023) << 6;
          break;
        case 6:
          output += f(current | (c >> 9));
          current = (c&511) << 7;
          break;
        case 7:
          output += f(current | (c >> 8));
          current = (c&255) << 8;
          break;
        case 8:
          output += f(current | (c >> 7));
          current = (c&127) << 9;
          break;
        case 9:
          output += f(current | (c >> 6));
          current = (c&63) << 10;
          break;
        case 10:
          output += f(current | (c >> 5));
          current = (c&31) << 11;
          break;
        case 11:
          output += f(current | (c >> 4));
          current = (c&15) << 12;
          break;
        case 12:
          output += f(current | (c >> 3));
          current = (c&7) << 13;
          break;
        case 13:
          output += f(current | (c >> 2));
          current = (c&3) << 14;
          break;
        case 14:
          output += f(current | (c >> 1));
          current = (c&1) << 15;
          break;
        case 15:
          output += f(current | c);
          status=0;
          break;
      }
      
      
      i++;
    }
    
    return LZString.decompress(output);
    //return output;
    
  },


  
  compress: function (uncompressed) {
    if (uncompressed == null) return "";
    var i, value,
        context_dictionary= {},
        context_dictionaryToCreate= {},
        context_c="",
        context_wc="",
        context_w="",
        context_enlargeIn= 2, // Compensate for the first entry which should not count
        context_dictSize= 3,
        context_numBits= 2,
        context_data_string="", 
        context_data_val=0, 
        context_data_position=0,
        ii,
        f=LZString._f;
    
    for (ii = 0; ii < uncompressed.length; ii += 1) {
      context_c = uncompressed.charAt(ii);
      if (!Object.prototype.hasOwnProperty.call(context_dictionary,context_c)) {
        context_dictionary[context_c] = context_dictSize++;
        context_dictionaryToCreate[context_c] = true;
      }
      
      context_wc = context_w + context_c;
      if (Object.prototype.hasOwnProperty.call(context_dictionary,context_wc)) {
        context_w = context_wc;
      } else {
        if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
          if (context_w.charCodeAt(0)<256) {
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1);
              if (context_data_position == 15) {
                context_data_position = 0;
                context_data_string += f(context_data_val);
                context_data_val = 0;
              } else {
                context_data_position++;
              }
            }
            value = context_w.charCodeAt(0);
            for (i=0 ; i<8 ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == 15) {
                context_data_position = 0;
                context_data_string += f(context_data_val);
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }
          } else {
            value = 1;
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1) | value;
              if (context_data_position == 15) {
                context_data_position = 0;
                context_data_string += f(context_data_val);
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = 0;
            }
            value = context_w.charCodeAt(0);
            for (i=0 ; i<16 ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == 15) {
                context_data_position = 0;
                context_data_string += f(context_data_val);
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }
          }
          context_enlargeIn--;
          if (context_enlargeIn == 0) {
            context_enlargeIn = Math.pow(2, context_numBits);
            context_numBits++;
          }
          delete context_dictionaryToCreate[context_w];
        } else {
          value = context_dictionary[context_w];
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == 15) {
              context_data_position = 0;
              context_data_string += f(context_data_val);
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }
          
          
        }
        context_enlargeIn--;
        if (context_enlargeIn == 0) {
          context_enlargeIn = Math.pow(2, context_numBits);
          context_numBits++;
        }
        // Add wc to the dictionary.
        context_dictionary[context_wc] = context_dictSize++;
        context_w = String(context_c);
      }
    }
    
    // Output the code for w.
    if (context_w !== "") {
      if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
        if (context_w.charCodeAt(0)<256) {
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1);
            if (context_data_position == 15) {
              context_data_position = 0;
              context_data_string += f(context_data_val);
              context_data_val = 0;
            } else {
              context_data_position++;
            }
          }
          value = context_w.charCodeAt(0);
          for (i=0 ; i<8 ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == 15) {
              context_data_position = 0;
              context_data_string += f(context_data_val);
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }
        } else {
          value = 1;
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1) | value;
            if (context_data_position == 15) {
              context_data_position = 0;
              context_data_string += f(context_data_val);
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = 0;
          }
          value = context_w.charCodeAt(0);
          for (i=0 ; i<16 ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == 15) {
              context_data_position = 0;
              context_data_string += f(context_data_val);
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }
        }
        context_enlargeIn--;
        if (context_enlargeIn == 0) {
          context_enlargeIn = Math.pow(2, context_numBits);
          context_numBits++;
        }
        delete context_dictionaryToCreate[context_w];
      } else {
        value = context_dictionary[context_w];
        for (i=0 ; i<context_numBits ; i++) {
          context_data_val = (context_data_val << 1) | (value&1);
          if (context_data_position == 15) {
            context_data_position = 0;
            context_data_string += f(context_data_val);
            context_data_val = 0;
          } else {
            context_data_position++;
          }
          value = value >> 1;
        }
        
        
      }
      context_enlargeIn--;
      if (context_enlargeIn == 0) {
        context_enlargeIn = Math.pow(2, context_numBits);
        context_numBits++;
      }
    }
    
    // Mark the end of the stream
    value = 2;
    for (i=0 ; i<context_numBits ; i++) {
      context_data_val = (context_data_val << 1) | (value&1);
      if (context_data_position == 15) {
        context_data_position = 0;
        context_data_string += f(context_data_val);
        context_data_val = 0;
      } else {
        context_data_position++;
      }
      value = value >> 1;
    }
    
    // Flush the last char
    while (true) {
      context_data_val = (context_data_val << 1);
      if (context_data_position == 15) {
        context_data_string += f(context_data_val);
        break;
      }
      else context_data_position++;
    }
    return context_data_string;
  },
  
  decompress: function (compressed) {
    if (compressed == null) return "";
    if (compressed == "") return null;
    var dictionary = [],
        next,
        enlargeIn = 4,
        dictSize = 4,
        numBits = 3,
        entry = "",
        result = "",
        i,
        w,
        bits, resb, maxpower, power,
        c,
        f = LZString._f,
        data = {string:compressed, val:compressed.charCodeAt(0), position:32768, index:1};
    
    for (i = 0; i < 3; i += 1) {
      dictionary[i] = i;
    }
    
    bits = 0;
    maxpower = Math.pow(2,2);
    power=1;
    while (power!=maxpower) {
      resb = data.val & data.position;
      data.position >>= 1;
      if (data.position == 0) {
        data.position = 32768;
        data.val = data.string.charCodeAt(data.index++);
      }
      bits |= (resb>0 ? 1 : 0) * power;
      power <<= 1;
    }
    
    switch (next = bits) {
      case 0: 
          bits = 0;
          maxpower = Math.pow(2,8);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = 32768;
              data.val = data.string.charCodeAt(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }
        c = f(bits);
        break;
      case 1: 
          bits = 0;
          maxpower = Math.pow(2,16);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = 32768;
              data.val = data.string.charCodeAt(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }
        c = f(bits);
        break;
      case 2: 
        return "";
    }
    dictionary[3] = c;
    w = result = c;
    while (true) {
      if (data.index > data.string.length) {
        return "";
      }
      
      bits = 0;
      maxpower = Math.pow(2,numBits);
      power=1;
      while (power!=maxpower) {
        resb = data.val & data.position;
        data.position >>= 1;
        if (data.position == 0) {
          data.position = 32768;
          data.val = data.string.charCodeAt(data.index++);
        }
        bits |= (resb>0 ? 1 : 0) * power;
        power <<= 1;
      }

      switch (c = bits) {
        case 0: 
          bits = 0;
          maxpower = Math.pow(2,8);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = 32768;
              data.val = data.string.charCodeAt(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }

          dictionary[dictSize++] = f(bits);
          c = dictSize-1;
          enlargeIn--;
          break;
        case 1: 
          bits = 0;
          maxpower = Math.pow(2,16);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = 32768;
              data.val = data.string.charCodeAt(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }
          dictionary[dictSize++] = f(bits);
          c = dictSize-1;
          enlargeIn--;
          break;
        case 2: 
          return result;
      }
      
      if (enlargeIn == 0) {
        enlargeIn = Math.pow(2, numBits);
        numBits++;
      }
      
      if (dictionary[c]) {
        entry = dictionary[c];
      } else {
        if (c === dictSize) {
          entry = w + w.charAt(0);
        } else {
          return null;
        }
      }
      result += entry;
      
      // Add w+entry[0] to the dictionary.
      dictionary[dictSize++] = w + entry.charAt(0);
      enlargeIn--;
      
      w = entry;
      
      if (enlargeIn == 0) {
        enlargeIn = Math.pow(2, numBits);
        numBits++;
      }
      
    }
  }
};

if( typeof module !== 'undefined' && module != null ) {
  module.exports = LZString
}

